# This file is specific to the mobans repository.
# If this comment appears elsewhere, .moban.yaml should
# be using constants.ps1.jj2 instead of constants.ps1

New-Variable -Scope global -Name project_name -Value 'mobans'
New-Variable -Scope global -Name pip_version -Value '9.0.3'
New-Variable -Scope global -Name setuptools_version -Value '21.2.2'

$old_EAP = $ErrorActionPreference
$ErrorActionPreference = 'SilentlyContinue'
Export-ModuleMember -Variable name, pip_version, setuptools_version
$ErrorActionPreference = $old_EAP
